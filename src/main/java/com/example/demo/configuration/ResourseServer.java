package com.example.demo.configuration;


import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;

@Configuration
@EnableResourceServer
//@Order(SecurityProperties.IGNORED_ORDER)
public class ResourseServer extends ResourceServerConfigurerAdapter {


    @Override
    public void configure(HttpSecurity http) throws Exception {


        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()

//                .antMatchers(HttpMethod.POST,"/api/private/**").access("#oauth2.hasScope('write')")
                .anyRequest()
                .authenticated();

        http.authorizeRequests().antMatchers("/client", "/").permitAll();

//        http.requestMatchers().antMatchers("/login","/oauth/authorize")
//                .and().authorizeRequests().anyRequest().authenticated().and().formLogin().permitAll();
//        http.authorizeRequests().antMatchers(HttpMethod.DELETE).access("#oauth2.hasScope('trust')").anyRequest().authenticated();
    }






}
