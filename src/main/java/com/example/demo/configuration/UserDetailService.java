package com.example.demo.configuration;

import com.example.demo.model.User;
import com.example.demo.repository.UserReporsitory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailService implements UserDetailsService {



    @Autowired
    private UserReporsitory userReporsitory;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User users = userReporsitory.findByUsername(username);

        if(users == null)

           throw new UsernameNotFoundException("User Not found");

      return users;
    }
}
