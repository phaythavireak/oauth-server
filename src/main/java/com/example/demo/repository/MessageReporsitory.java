package com.example.demo.repository;

import com.example.demo.model.Message;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface MessageReporsitory extends CrudRepository<Message,Integer>
{
    @Override
    <S extends Message> S save(S entity);

    @Override
    Iterable<Message> findAll();


    @Override
    Optional<Message> findById(Integer integer);
}
