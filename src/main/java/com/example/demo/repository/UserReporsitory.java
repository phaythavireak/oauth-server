package com.example.demo.repository;


import com.example.demo.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserReporsitory extends CrudRepository<User,Integer> {


    public User findByUsername(String username);



}
