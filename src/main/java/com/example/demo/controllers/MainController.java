package com.example.demo.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class MainController {

    @GetMapping("/client")
    public String test(@RequestParam("code") String code) {
        System.out.println(code);
        return "redirect:/oauth/token?grant_type=authorization_code" +
                "&client_id=client1&client_secret=secret&code=" + code + "&redirect_url=www.facebook.com";
    }
}
