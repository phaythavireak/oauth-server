package com.example.demo.restController;


import com.example.demo.model.Message;
import com.example.demo.repository.MessageReporsitory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api/private")
public class MessageRestController {




    @Autowired
        private MessageReporsitory messageReporsitory;



    @RequestMapping("/all")
    public Map<String,Object> getall()
    {

        Map<String,Object> map = new HashMap<>();
        List<Message> messages = (List<Message>) messageReporsitory.findAll();

        if (messages != null)
        {
            map.put("Data",messages);
        }
        else
        {
            map.put("Status",false);
        }

        return map;
    }
    @RequestMapping("/find/{id}")
    public Map<String,Object> findbyid(@PathVariable int id)
    {

        Map<String,Object> map = new HashMap<>();
        Optional<Message> messages = messageReporsitory.findById(id);

        if (messages != null)
        {
            map.put("Data",messages);
        }
        else
        {
            map.put("Status",false);
        }

        return map;
    }
    @PostMapping("/save")
   // @PreAuthorize("#oauth2.hasScope('read')")

    public Map<String,Object> save(@RequestBody Message message)
    {

        Map<String,Object> map = new HashMap<>();
        Message messages = messageReporsitory.save(message);

        if (messages != null)
        {
            map.put("Data",messages);
        }
        else
        {
            map.put("Status",false);
        }

        return map;
    }





    //@PreAuthorize("#oauth2.hasScope('read')")
    @RequestMapping(value = "/delete/{id}",method = RequestMethod.DELETE)
    public Map<String,Object> delete(@PathVariable int id)
    {

        Map<String,Object> map = new HashMap<>();
        messageReporsitory.deleteById(id);

        map.put("Status",true);
        map.put("SMS","Delete Successfully");

        return map;
    }





}
