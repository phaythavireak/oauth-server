INSERT INTO oauth_client_details(client_id, client_secret, scope, authorized_grant_types, web_server_redirect_uri, authorities, access_token_validity, refresh_token_validity, additional_information, autoapprove)
VALUES('client1','{noop}sceret', 'read,write,trust', 'password,refresh_token,authorization_code,implicit,client_credentials','http://localhost:9090/client', 'ROLE_ADMIN,ROLE_USER', 120, 240, NULL, true);

INSERT INTO oauth_client_details(client_id, client_secret, scope, authorized_grant_types, web_server_redirect_uri, authorities, access_token_validity, refresh_token_validity, additional_information, autoapprove)
VALUES('client2','{noop}screret', 'read,write', 'password,implicit',NULL, 'ROLE_ADMIN,ROLE_USER', 120, 240, NULL, true);

INSERT INTO oauth_client_details(client_id, client_secret, scope, authorized_grant_types, web_server_redirect_uri, authorities, access_token_validity, refresh_token_validity, additional_information, autoapprove)
VALUES('client3','{noop}password', 'bar,read,write', 'password,refresh_token,authorization_code',NULL, 'ROLE_ADMIN', 120, 240, NULL, true);






-- =================== user Data ======================





INSERT INTO user_table(username,password,role,activated) VALUES ('user','{noop}user','ROLE_ADMIN',true);
INSERT INTO user_table(username,password,role,activated) VALUES ('kheang','{noop}password','ROLE_USER',true);
INSERT INTO user_table(username,password,role,activated) VALUES ('thon','{noop}password','ROLE_USER',false);

INSERT INTO message(name) VALUES ('thon');
INSERT INTO message(name) VALUES ('that');
INSERT INTO message(name) VALUES ('kheang');












